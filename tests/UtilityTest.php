<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 28/04/2016
 * Time: 17:18
 */

namespace studentTest;

use Project\Data\Student;

class UtilityTest extends \PHPUnit_Framework_TestCase
{
    public function testUtilityChangesControllerInputToDifferentOutput()
    {
        // Arrange
        $student = new Student();
        $student->setId('1');
        $expectedResult = '1';

        // Act
        $result = $student->getId();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }
}
