<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 28/04/2016
 * Time: 1:18
 */

namespace ProjectTest;

use Project\Data\Member;
use Project\Data\Student;

class MemberTest extends \PHPUnit_Framework_TestCase
{
    public function testGetMemberFromId()
    {
        // Arrange
        $member = Member::getOneById("1");
        $expectedResult = '1';

        // Act
        $result = $member->getId();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetMemberIdGetsSameId()
    {
        // Arrange
        $member = new Member();
        $member->setId('1');
        $expectedResult = '1';

        // Act
        $result = $member->getId();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetFirstnameFromId()
    {
        // Arrange
        $member = Member::getOneById("1");
        $expectedResult = 'Colin';

        // Act
        $result = $member->getFirstname();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetPasswordGetsHashedPassword()
    {
        // Arrange
        $member = new Member();
        $member->setPassword('Password');

        // Act
        $result = $member->getPassword();

        // Assert
        $this->assertTrue(password_verify($result, PASSWORD_DEFAULT));
    }

    public function testSetFirstnameThenGetSameFirstname()
    {
        // Arrange
        $member = new Member();
        $member->setFirstname('name');
        $expectedResult = 'name';

        // Act
        $result = $member->getFirstname();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetLastnameFromId()
    {
        // Arrange
        $member = Member::getOneById("1");
        $expectedResult = 'Forrester';

        // Act
        $result = $member->getLastname();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetLastnameGetsSameLastname()
    {
        // Arrange
        $member = new Member();
        $member->setLastname('name');
        $expectedResult = 'name';

        // Act
        $result = $member->getLastname();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetUsernameFromId()
    {
        // Arrange
        $member = Member::getOneById("1");
        $expectedResult = 'Colyf';

        // Act
        $result = $member->getUsername();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetUsernameGetsSameUsername()
    {
        // Arrange
        $member = new Member();
        $member->setUsername('name');
        $expectedResult = 'name';

        // Act
        $result = $member->getUsername();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetTitleFromId()
    {
        // Arrange
        $member = Member::getOneById("1");
        $expectedResult = 'Mr.';

        // Act
        $result = $member->getTitle();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetTitleGetsSameTitle()
    {
        // Arrange
        $member = new Member();
        $member->setTitle('title');
        $expectedResult = 'title';

        // Act
        $result = $member->getTitle();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetGenderFromId()
    {
        // Arrange
        $member = Member::getOneById("1");
        $expectedResult = 'Male';

        // Act
        $result = $member->getGender();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGenderGetsSameGender()
    {
        // Arrange
        $member = new Member();
        $member->setGender('male');
        $expectedResult = 'male';

        // Act
        $result = $member->getGender();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetPhotoUrlFromId()
    {
        // Arrange
        $member = Member::getOneById("1");
        $expectedResult = 'member/1.jpg';

        // Act
        $result = $member->getPhotoUrl();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetPhotoUrlGetsSamePhotoUrl()
    {
        // Arrange
        $member = new Member();
        $member->setPhotoUrl('member/1.jpg');
        $expectedResult = 'name';

        // Act
        $result = $member->getPhotoUrl();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetRoleFromId()
    {
        // Arrange
        $member = Member::getOneById("1");
        $expectedResult = '1';

        // Act
        $result = $member->getRole();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetRoleGetsSameRole()
    {
        // Arrange
        $member = new Member();
        $member->setRole('1');
        $expectedResult = '1';

        // Act
        $result = $member->getRole();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetPastFromId()
    {
        // Arrange
        $member = Member::getOneById("1");
        $expectedResult = '0';

        // Act
        $result = $member->getPast();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetPastGetsSamePast()
    {
        // Arrange
        $member = new Member();
        $member->setPast('1');
        $expectedResult = '1';

        // Act
        $result = $member->getPast();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetStudentIdWhenMemberIsStudent(){
        // Arrange
        $member = Member::getOneById("1");
        $student = Student::getOneById($member->getId());
        $expectedResult = 'B00075660';

        // Act
        $result = $student->getStudentNumber();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetNullWhenMemberIsStudent(){
    // Arrange
        $member = Member::getOneById("3");
        $student = Student::getOneById($member->getId());
        $expectedResult = null;

        // Act
        $result = $student;

        // Assert
        $this->assertNull($result);
    }

    public function testGetMemberByFindMatchingUsernameAndPasswordGetsMember(){
        // Arrange
        $member = Member::canFindMatchingUsernameAndPassword('Colyf', 'Password');
        $expectedResult = Member::getOneByUsername('Colyf');

        // Act
        $result = $member;

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetNullFindMatchingUsernameAndPasswordDoesNotFindMatch(){
        // Arrange
        $result = Member::canFindMatchingUsernameAndPassword('Col', 'Password');

        // Assert
        $this->assertNull($result);
    }

    public function testGetMemberById(){
        // Arrange
        $member = Member::getOneById('1');
        $expectedResult = Member::getOneByUsername('Colin');

        // Act
        $result = $member;

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetMemberInnerJoinStudentGetsMemberAndStudentDetails(){
        // Arrange
        $member = Member::getStudentInnerJoinMemberById('1');
        $student = Student::getOneById('1');
        $expectedResult = $student->getMemberId();

        // Act
        $result = $member->getId();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetNullWhenInnerJoinStudentIsNotStudent(){
        // Arrange
        $result = Member::getStudentInnerJoinMemberById('3');
        $expectedResult = null;

        // Assert
        $this->assertNull($result);
    }

}
