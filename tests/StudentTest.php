<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 28/04/2016
 * Time: 17:18
 */

namespace studentTest;

use Project\Data\Student;

class StudentTest extends \PHPUnit_Framework_TestCase
{
    public function testSetGetId()
    {
        // Arrange
        $student = new Student();
        $student->setId('1');
        $expectedResult = '1';

        // Act
        $result = $student->getId();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetMemberId()
    {
        // Arrange
        $student = new Student();
        $student->setMemberId('1');
        $expectedResult = '1';

        // Act
        $result = $student->getMemberId();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetCatagory()
    {
        // Arrange
        $student = new Student();
        $student->setStudentNumber('B00075660');
        $expectedResult = 'B00075660';

        // Act
        $result = $student->getStudentNumber();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }
}
