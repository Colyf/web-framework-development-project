<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 28/04/2016
 * Time: 17:18
 */

namespace ProjectTest;

use Project\Data\Project;

class ProjectTest extends \PHPUnit_Framework_TestCase
{
    public function testSetGetId()
    {
        // Arrange
        $project = new Project();
        $project->setId('1');
        $expectedResult = '1';

        // Act
        $result = $project->getId();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetDescription()
    {
        // Arrange
        $project = new Project();
        $project->setDescription('Create a website for the GGG.');
        $expectedResult = 'Create a website for the GGG.';

        // Act
        $result = $project->getDescription();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetFileLink()
    {
        // Arrange
        $project = new Project();
        $project->setFileLink('files/Project_Graphics_and_Gaming_Research_Group.zip');
        $expectedResult = 'files/Project_Graphics_and_Gaming_Research_Group.zip';

        // Act
        $result = $project->getFilelink();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetUploadTime()
    {
        // Arrange
        $project = new Project();
        $project->setUploadTime('2016-04-12 12:07:41');
        $expectedResult = '2016-04-12 12:07:41';

        // Act
        $result = $project->getUploadTime();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetPast()
    {
        // Arrange
        $project = new Project();
        $project->setPast('0');
        $expectedResult = '0';

        // Act
        $result = $project->getPast();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetTitle()
    {
        // Arrange
        $project = new Project();
        $project->setTitle('Project');
        $expectedResult = 'Project';

        // Act
        $result = $project->getTitle();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetCreatorId()
    {
        // Arrange
        $project = new Project();
        $project->setCreatorId('1');
        $expectedResult = '1';

        // Act
        $result = $project->getCreatorId();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetImgPath()
    {
        // Arrange
        $project = new Project();
        $project->setImgPath('img/project/1.jpg');
        $expectedResult = 'img/project/1.jpg';

        // Act
        $result = $project->getImgPath();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }
}
