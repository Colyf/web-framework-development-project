<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 28/04/2016
 * Time: 17:18
 */

namespace PublicationTest;

use Project\Data\Publication;

class PublicationTest extends \PHPUnit_Framework_TestCase
{
    public function testSetGetId()
    {
        // Arrange
        $publication = new Publication();
        $publication->setId('1');
        $expectedResult = '1';

        // Act
        $result = $publication->getId();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetDescription()
    {
        // Arrange
        $publication = new Publication();
        $publication->setDescription('Description');
        $expectedResult = 'Description';

        // Act
        $result = $publication->getDescription();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetCatagory()
    {
        // Arrange
        $publication = new Publication();
        $publication->setCatagory('Web Framework Development');
        $expectedResult = 'Web Framework Development';

        // Act
        $result = $publication->getCatagory();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetUploaded()
    {
        // Arrange
        $publication = new Publication("1");
        $publication->setUploaded('2016-04-28 20:58:39');
        $expectedResult = '2016-04-28 20:58:39';

        // Act
        $result = $publication->getUploaded();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetFileLink()
    {
        // Arrange
        $publication = new Publication("1");
        $publication->setFileLink('files/project/1.zip');
        $expectedResult = 'files/project/1.zip';

        // Act
        $result = $publication->getFileLink();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function testSetGetTitle()
    {
        // Arrange
        $publication = new Publication("1");
        $publication->setTitle('Title');
        $expectedResult = 'Title';

        // Act
        $result = $publication->getTitle();

        // Assert
        $this->assertEquals($expectedResult, $result);
    }
}
