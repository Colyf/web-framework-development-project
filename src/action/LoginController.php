<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 08/04/2016
 * Time: 20:09
 */
namespace Project\action;



use Project\Data\Member;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Login Controller for logging in and signing out of the website
 * Class LoginController used to handle the login of members to the website.
 * @package Project\action
 */
class LoginController
{
    /**
     * Handles the request for the login page
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function loginAction(Request $request, Application $app)
    {
        $user = $this->getUserFromSession($app);

        $argsArray = [
            'user' => $user,
        ];

        $template = 'login';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Handles the request for the logout, unsets the user from the session
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function logoutAction(Request $request, Application $app)
    {
        if ($app['session']->get('user') != null) {
            $app['session']->set('user', null);
        }

        $argsArray = [
            'logout' => 'true',
        ];


        $template = 'login';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Handles the request for process login, checks if the username and password are a match.
     * Then sets up the session variables,
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function processLoginAction(Request $request, Application $app)
    {
        // default is bad login
        $isLoggedIn = false;

        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

        // search for user with username in repository
        $isLoggedIn = Member::canFindMatchingUsernameAndPassword($username, $password);

        // action depending on login success
        if ($isLoggedIn) {
            $user = Member::getOneByUsername($username);
            $app['session']->set('user', array(
                                                'id' => $user->getId(),
                                                'name' => $user->getFirstname(),
                                                'role' => $user->getrole()
            ));

            $argsArray = [
                'user' => $this->getUserFromSession($app),
            ];

            $template = 'index';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        } else {
            $argsArray = [
                'failed' => 'failed',
            ];
            $template = 'login';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        }
    }

    /**
     * Handles the request for getting the user from the session.
     * Returns either an array representing the user or null.
     * @param Application $app
     * @return null
     */
    private function getUserFromSession(Application $app)
    {
        if ($app['session']->get('user') != null) {
            $user = $app['session']->get('user');
        } else {
            $user = null;
        }
        return $user;
    }
}
