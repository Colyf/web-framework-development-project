<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 08/04/2016
 * Time: 20:09
 */

namespace Project\action;

use Project\Data\Member;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller to handle the Member Class
 * Class MemberController
 * @package Project\action
 */
class MemberController
{
    /**
     * Controls the action for inserting a new member
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function insertMemberAction(Request $request, Application $app)
    {
        $user = $this->getUserFromSession($app);

        $added = false;

        //retrieve user details
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
        $lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
        $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
        $gender = filter_input(INPUT_POST, 'gender', FILTER_SANITIZE_STRING);
        $photoUrl = filter_input(INPUT_POST, 'photoUrl', FILTER_SANITIZE_STRING);
        $role = filter_input(INPUT_POST, 'role', FILTER_SANITIZE_NUMBER_INT);
        $past = filter_input(INPUT_POST, 'past', FILTER_SANITIZE_NUMBER_INT);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

        $type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING);

        $member = new Member();

        $member->setId($id);
        $member->setFirstname($firstname);
        $member->setLastname($lastname);
        $member->setTitle($title);
        $member->setGender($gender);
        $member->setPhotoUrl($photoUrl);
        $member->setUsername($username);
        $member->setPassword($password);
        $member->setRole($role);
        $member->setPast($past);

        Member::insert($member);
        $members = Member::getAll();

        $argsArray = [
            'new' => $member,
            'members' => $members,
            'user' => $user,
        ];

        $template = 'members';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Contols the action to display the add member form
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function addMemberFormAction(Request $request, Application $app)
    {
        $user = $this->getUserFromSession($app);

        $argsArray = [
            'user' => $user,
            'add' => ''
        ];

        $template = 'memberDetails';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Controls the action for processing edit member
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function processEditMemberAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);

        //retrieve user details
        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
        $lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
        $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
        $gender = filter_input(INPUT_POST, 'gender', FILTER_SANITIZE_STRING);
        //$fileLink = filter_input(INPUT_POST, 'fileLink', FILTER_SANITIZE_STRING);
        $role = filter_input(INPUT_POST, 'role', FILTER_SANITIZE_NUMBER_INT);
        $past = filter_input(INPUT_POST, 'past', FILTER_SANITIZE_NUMBER_INT);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);


        $member = Member::getOneById($id);
        $member->setId($id);
        $member->setFirstname($firstname);
        $member->setLastname($lastname);
        $member->setTitle($title);
        $member->setGender($gender);
        //$member->setPhotoUrl($fileLink);
        $member->setUsername($username);
        $member->setPassword($password);
        $member->setRole($role);
        $member->setPast($past);

        Member::update($member);

        $argsArray = [
            'member' => $member,
            'user' => $user,
            'edited' => $member->getFirstname(),
        ];

        $template = 'memberDetails';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Controls the action for displaying the edit member
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function editMemberAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);

        if (!$this->checkPermission($user, $id)) {
            $argsArray = [
                'user' => $user,
                'errorMessage' => 'Permission not allowed, You cannot edit a member unless it is you or you are a part of the Admin team',
            ];
            return $app['twig']->render('error' . '.html.twig', $argsArray);
        } else {
            $edit = Member::getOneById($id);

            if($edit->getRole() == Member::ROLE_STUDENT){
                $edit = Member::getStudentInnerJoinMemberById($id);
            }

            $argsArray = [
                'user' => $user,
                'edit' => $edit,
            ];

            $template = 'memberDetails';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        }
    }

    /**
     * Controls the action for deleting a member
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function deleteMemberAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);
        if (!$this->checkPermission($user, $id)) {
            $argsArray = [
                'user' => $user,
                'errorMessage' => 'Permission not allowed, You cannot edit a member unless it is you or you are a part of the Admin team',
            ];
            return $app['twig']->render('error' . '.html.twig', $argsArray);
        } else {
            $member = Member::getOneById($id);

            Member::delete($id);

            $members = Member::getAll();

            $argsArray = [
                'delete' => $member,
                'user' => $user,
                'members' => $members,
            ];

            $template = 'members';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        }
    }

    /**
     * Controls the action for adding or removing Past on a member
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function pastMemberAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);
        $member = Member::getOneById($id);
        if (!$this->checkPermission($user, $id)) {
            $argsArray = [
                'user' => $user,
                'errorMessage' => 'Permission not allowed, Only Admin can past a member, or the member himself.',
            ];
            return $app['twig']->render('error' . '.html.twig', $argsArray);
        } else {
            if ($member->getPast(0)) {
                $member->setPast(1);
            } else {
                $member->setPast(0);
            }

            Member::update($member);

            $argsArray = [
                'past' => $member->getTitle(),
                'member' => $member,
                'user' => $user,
            ];

            $template = 'memberDetails';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        }
    }

    /**
     * Checks for user which is stored in the session
     * Returns an array of the user or null
     * @param Application $app
     * @return null
     */
    private function getUserFromSession(Application $app)
    {
        if ($app['session']->get('user') != null) {
            $user = $app['session']->get('user');
        } else {
            $user = null;
        }
        return $user;
    }

    /**
     * Check permissions to see if the user is allowed to edit
     * If the user is admin - True
     * If user is editing their own profile - True
     * else - False
     * @param $user
     * @param $id
     * @return bool
     */
    private function checkPermission($user, $id)
    {
        if ($user['id'] == $id) {
            return true;
        } elseif ($user['role'] == Member::ROLE_ADMIN) {
            return true;
        }
        return false;
    }
}
