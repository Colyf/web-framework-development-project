<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 28/04/2016
 * Time: 17:49
 */

namespace Project\action;

use Project\Data\Publication;
use Project\Data\Member;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Project Controller class is used for managing the publication elements of the website.
 * Handles deleting, updating, adding and reading of the publications.
 * Class PublicationController
 * @package Project\action
 */
class PublicationController
{

    /**
     * Handles the request for inserting a publication into the Database
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function insertPublicationAction(Request $request, Application $app)
    {
        $user = $this->getUserFromSession($app);

        //retrieve user details
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
        $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_NUMBER_INT);
        $catagory = filter_input(INPUT_POST, 'catagory', FILTER_SANITIZE_STRING);
        $fileLink = filter_input(INPUT_POST, 'fileLink', FILTER_SANITIZE_STRING);

        $publication = new Publication();

        $publication->setId($id);
        $publication->setTitle($title);
        $publication->setCatagory($catagory);
        $publication->setDescription($description);
        $publication->setFileLink($fileLink);

        Publication::insert($publication);
        $publications = Publication::getAll();

        $argsArray = [
            'new' => $publication,
            'publications' => $publications,
            'user' => $user,
        ];

        $template = 'publications';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Contols the action to display the add publication form
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function addPublicationFormAction(Request $request, Application $app)
    {
        $user = $this->getUserFromSession($app);

        if (!$this->checkIsMember($user)) {
            $argsArray = [
                'user' => $user,
                'errorMessage' => 'Permission not allowed, Only Admin or a Member can add a Publications.',
            ];
            return $app['twig']->render('error' . '.html.twig', $argsArray);
        } else {


        $argsArray = [
            'user' => $user,
            'add' => '',
        ];

        $template = 'publicationDetails';
        return $app['twig']->render($template . '.html.twig', $argsArray);
        }
    }


    /**
     * Handles the request for and checking the edit publications form
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function processEditPublicationAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);

        if (!$this->checkIsMember($user)) {
            $argsArray = [
                'user' => $user,
                'errorMessage' => 'Permission not allowed, Only Admin or a Member can edit Publications.',
            ];
            return $app['twig']->render('error' . '.html.twig', $argsArray);
        } else {

            //retrieve publication details
            $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $catagory = filter_input(INPUT_POST, 'catagory', FILTER_SANITIZE_STRING);
            $uploaded = filter_input(INPUT_POST, 'uploaded', FILTER_SANITIZE_STRING);
            $fileLink = filter_input(INPUT_POST, 'fileLink', FILTER_SANITIZE_STRING);

            $publication = new Publication();

            $publication->setId($id);
            $publication->setTitle($title);
            $publication->setCatagory($catagory);
            $publication->setDescription($description);
            $publication->setUploaded($uploaded);
            $publication->setFileLink($fileLink);

            Publication::update($publication);

            $argsArray = [
                'publication' => $publication,
                'user' => $user,
                'edited' => $publication->getTitle(),
            ];

            $template = 'publicationDetails';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        }
    }

    /**
     * Handles the request to edit a publication in the Database
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function editPublicationAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);

        if (!$this->checkIsMember($user)) {
            $argsArray = [
                'user' => $user,
                'errorMessage' => 'Permission not allowed, Only Admin or a Member can edit Publications.',
            ];
            return $app['twig']->render('error' . '.html.twig', $argsArray);
        } else {
            $publication = Publication::getOneById($id);

            $argsArray = [
                'user' => $user,
                'edit' => $publication,
            ];

            $template = 'publicationDetails';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        }
    }

    /**
     * Handles the request for deleting a publication from the Database
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function deletePublicationAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);

        if (!$this->checkIsMember($user)) {
            $argsArray = [
                'user' => $user,
                'errorMessage' => 'Permission not allowed, Only Admin or a Member can delete Publications.',
            ];
            return $app['twig']->render('error' . '.html.twig', $argsArray);
        } else {
            $publication = Publication::getOneById($id);

            Publication::delete($id);

            $publications = Publication::getAll();

            $argsArray = [
                'delete' => $publication,
                'user' => $user,
                'publications' => $publications,
            ];

            $template = 'publications';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        }

    }
}
