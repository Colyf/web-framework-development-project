<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 08/04/2016
 * Time: 20:09
 */
namespace Project\action;

use Project\Data\Member;
use Project\Data\Project;
use Project\Data\Publication;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Main Controller for the website handling standard(no login required) Actions
 *
 * Class MainController this controller handles all requests for standard users with no login, such as the navigation
 * bar. eg. index, about, list of members, projects and publications.
 *
 * @package Project\action
 */
class MainController
{
    /**
     * Handles the request for the index page
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function indexAction(Request $request, Application $app)
    {
        $user = $this->getUserFromSession($app);

        $argsArray = [
        'user' => $user,
        ];

        $template = 'index';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Handles the request for the about page
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function aboutAction(Request $request, Application $app)
    {
        $user = $this->getUserFromSession($app);

        $argsArray = [
        'user' => $user,
        ];

        $template = 'about';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Handles the request for the members page
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function membersAction(Request $request, Application $app)
    {
        $user = $this->getUserFromSession($app);

        $members = Member::getAll();

        $argsArray = [
          'members' => $members,
          'user' => $user,
        ];

        $template = 'members';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Handles the request for the member details page
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function memberDetailsAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);
        $member = Member::getOneById($id);

        if($member->getRole() == Member::ROLE_STUDENT){
            $member = Member::getStudentInnerJoinMemberById($member->getId());
        }
        $argsArray = [
            'member' => $member,
            'user' => $user,
        ];

        $template = 'memberDetails';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Handles the request for the projects page
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function projectsAction(Request $request, Application $app)
    {
        $user = $this->getUserFromSession($app);
        $projects = Project::getAll();

        $argsArray = [
          'projects' => $projects,
          'user' => $user,
      ];

        $template = 'projects';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Handles the request for the project details page
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function projectDetailsAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);
        $project = Project::getOneById($id);

        $argsArray = [
            'project' => $project,
            'user' => $user,
        ];

        $template = 'projectDetails';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Handles the request for the publications page
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function publicationsAction(Request $request, Application $app)
    {
        $user = $this->getUserFromSession($app);

        $publications = Publication::getAll();

        $argsArray = [
        'publications' => $publications,
        'user' => $user,
    ];

        $template = 'publications';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Handles the request for the publication details page
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function publicationDetailsAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);
        $publication = Publication::getOneById($id);

        $argsArray = [
            'publication' => $publication,
            'user' => $user,
        ];

        $template = 'publicationDetails';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
     * Handles the request for the search...
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function searchAction(Request $request, Application $app)
    {
        $publicationRepository = new DatabaseTable('Publication', 'publication');
        $publications = $publicationRepository->getAll();

        // pass the publications loaded from the database
        // through the publication repository
        // to the publicaiton page
        $argsArray = [
            'publications' => $publications,
        ];


        $template = 'publications';
        return $app['twig']->render($template . '.html.twig', $argsArray);
    }

    /**
    * Handles the request when the page is not found.
    * @param \Silex\Application $app
    * @param             $message
    *
    * @return mixed
    */
    public static function error404(Application $app, $message)
    {
        $argsArray = [

        ];
        $templateName = '404';
        return $app['twig']->render($templateName . '.html.twig', $argsArray);
    }

    /**
     * Checks for user which is stored in the session
     * Returns an array of the user or null
     * @param Application $app
     * @return null
     */
    private function getUserFromSession(Application $app)
    {
        if ($app['session']->get('user') != null) {
            $user = $app['session']->get('user');
        } else {
            $user = null;
        }
        return $user;
    }
}
