<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 28/04/2016
 * Time: 17:49
 */

namespace Project\action;

use Project\Data\Project;
use Project\Data\Member;
use Project\Utility\Utility;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Project Controller class is used for managing the project elements of the website.
 * Handles deleting, updating, adding and reading of the projects.
 * Class ProjectController
 * @package Project\action
 */
class ProjectController
{
    /**
     * Handles the request for adding a project into the Database
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function addProjectAction(Request $request, Application $app)
    {
        $user = $this->getUserFromSession($app);

        if (!Utility::checkIsMember($user)) {
            $argsArray = [
                'user' => $user,
                'errorMessage' => 'Permission not allowed, Only Admin can edit a Project, or the creator himself.',
            ];
            return $app['twig']->render('error' . '.html.twig', $argsArray);
        } else {
            $added = false;

            //retrieve user details
            $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
            $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $fileLink = filter_input(INPUT_POST, 'fileLink', FILTER_SANITIZE_STRING);
            $past = filter_input(INPUT_POST, 'past', FILTER_SANITIZE_NUMBER_INT);
            $uploadTime = filter_input(INPUT_POST, 'uploadTime', FILTER_SANITIZE_STRING);
            $creatorId = filter_input(INPUT_POST, 'creatorId', FILTER_SANITIZE_NUMBER_INT);
            $imgPath = filter_input(INPUT_POST, 'imgPath', FILTER_SANITIZE_STRING);

            $project = new Project();

            $project->setId($id);
            $project->setTitle($title);
            $project->setDescription($description);
            $project->setFileLink($fileLink);
            $project->setPast($past);
            $project->setUploadTime($uploadTime);
            $project->setCreatorId($creatorId);
            $project->setImgPath($imgPath);

            Project::insert($project);
            $projects = Project::getAll();

            $argsArray = [
                'new' => $project,
                'projects' => $projects,
                'user' => $user,
            ];

            $template = 'projects';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        }

    }

    /**
     * Handles the processing of the Edit Projects Form
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function processEditProjectAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);
        $project = Project::getOneById($id);

        if (!$this->checkPermission($user, $project->getCreatorId())) {
            $argsArray = [
                'user' => $user,
                'errorMessage' => 'Permission not allowed, Only Admin can edit a Project, or the creator himself.',
            ];
            return $app['twig']->render('error' . '.html.twig', $argsArray);
        } else {

            $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $fileLink = filter_input(INPUT_POST, 'fileLink', FILTER_SANITIZE_STRING);
            $past = filter_input(INPUT_POST, 'past', FILTER_SANITIZE_NUMBER_INT);
            $uploadTime = filter_input(INPUT_POST, 'uploadTime', FILTER_SANITIZE_STRING);
            $creatorId = filter_input(INPUT_POST, 'creatorId', FILTER_SANITIZE_NUMBER_INT);
            $imgPath = filter_input(INPUT_POST, 'imgPath', FILTER_SANITIZE_STRING);

            $project = new Project();

            $project->setTitle($title);
            $project->setDescription($description);
            $project->setFileLink($fileLink);
            $project->setPast($past);
            $project->setUploadTime($uploadTime);
            $project->setCreatorId($creatorId);
            $project->setImgPath($imgPath);

            Project::update($project);

            $argsArray = [
                'project' => $project,
                'user' => $user,
                'edited' => $project->getTitle(),
            ];

            $template = 'projectDetails';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        }
    }

    /**
     * Handles the request for to edit the Project by id
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function editProjectAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);
        $project = Project::getOneById($id);

        if (!$this->checkPermission($user, $project->getCreatorId())) {
            $argsArray = [
                'user' => $user,
                'errorMessage' => 'Permission not allowed, Only Admin can edit a Project, or the creator himself.',
            ];
            return $app['twig']->render('error' . '.html.twig', $argsArray);
        } else {
            $argsArray = [
                'user' => $user,
                'edit' => $project,
            ];

            $template = 'projectDetails';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        }
    }

    /**
     * Handles the request for deleting an project by id
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function deleteProjectAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);
        $project = Project::getOneById($id);

        if (!$this->checkPermission($user, $project->getCreatorId())) {
            $argsArray = [
                'user' => $user,
                'errorMessage' => 'Permission not allowed, Only Admin can delete a Project, or the creator himself.',
            ];
            return $app['twig']->render('error' . '.html.twig', $argsArray);
        } else {
            Project::delete($id);

            $projects = Project::getAll();

            $argsArray = [
                'delete' => $project,
                'user' => $user,
                'projects' => $projects,
            ];

            $template = 'projects';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        }
    }

    /**
     * Handles the request to past a project
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function pastProjectAction(Request $request, Application $app, $id)
    {
        $user = $this->getUserFromSession($app);
        $project = Project::getOneById($id);

        if (!$this->checkPermission($user, $project->getCreatorId())) {
            $argsArray = [
                'user' => $user,
                'errorMessage' => 'Permission not allowed, Only Admin can past a Project, or the creator himself.',
            ];
            return $app['twig']->render('error' . '.html.twig', $argsArray);
        } else {
            if ($project->getPast(0)) {
                $project->setPast(1);
            } else {
                $project->setPast(0);
            }

            Project::update($project);

            $argsArray = [
                'past' => $project->getTitle(),
                'project' => $project,
                'user' => $user,
            ];

            $template = 'projectDetails';
            return $app['twig']->render($template . '.html.twig', $argsArray);
        }
    }

    /**
     * Checks for user which is stored in the session
     * Returns an array of the user or null
     * @param Application $app
     * @return null
     */
    private function getUserFromSession(Application $app)
    {
        if ($app['session']->get('user') != null) {
            $user = $app['session']->get('user');
        } else {
            $user = null;
        }
        return $user;
    }
}
