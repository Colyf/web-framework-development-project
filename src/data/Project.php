<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 09/04/2016
 * Time: 02:28
 */

namespace Project\data;

use Mattsmithdev\PdoCrud\DatabaseTable;

/**
 * Project Class represents the projects from the Database
 *
 * Class Project includes all the variables that exists in the Database for projects.
 * includes setters and getters for all elements
 *
 * @package Project\data
 */
class Project extends DatabaseTable
{

    /**
     * Project's id
     * @var
     */
    private $id;
    /**
     * Project's title
     * @var
     */
    private $title;
    /**
     * Project's description
     * @var
     */
    private $description;
    /**
     * Project's file link
     * @var
     */
    private $fileLink;
    /**
     * Project's past value
     * @var
     */
    private $past;
    /**
     * Project's upload time
     * @var
     */
    private $uploadTime;
    /**
     * Project's creator ID
     * @var
     */
    private $creatorId;
    /**
     * Project's image path
     * @var
     */
    private $imgPath;

    /**
     * Returns the projects image path
     * @return mixed
     */
    public function getImgPath()
    {
        return $this->imgPath;
    }

    /**
     * Sets the projects image path
     * @param mixed $imgPath
     */
    public function setImgPath($imgPath)
    {
        $this->imgPath = $imgPath;
    }

    /**
     * Returns the projects creator id
     * @return mixed
     */
    public function getCreatorId()
    {
        return $this->creatorId;
    }

    /**
     * Sets the projects creatorId
     * @param mixed $creatorId
     */
    public function setCreatorId($creatorId)
    {
        $this->creatorId = $creatorId;
    }

    /**
     * Returns the projects upload time
     * @return mixed
     */
    public function getUploadTime()
    {
        return $this->uploadTime;
    }

    /**
     * Sets the projects upload time
     * @param mixed $uploadTime
     */
    public function setUploadTime($uploadTime)
    {
        $this->uploadTime = $uploadTime;
    }


    /**
     * Returns the projects id
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the projects id
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the projects title
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the projects title
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the projects description
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the projects description
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the projects file link
     * @return mixed
     */
    public function getFileLink()
    {
        return $this->fileLink;
    }

    /**
     * Sets the projects file link
     * @param mixed $fileLink
     */
    public function setFileLink($fileLink)
    {
        $this->fileLink = $fileLink;
    }

    /**
     * Returns the projects past value
     * @return mixed
     */
    public function getPast()
    {
        return $this->past;
    }

    /**
     * Sets the projects past value
     * @param mixed $past
     */
    public function setPast($past)
    {
        $this->past = $past;
    }
}
