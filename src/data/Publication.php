<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 09/04/2016
 * Time: 02:33
 */

namespace Project\data;

use Mattsmithdev\PdoCrud\DatabaseTable;

/**
 * Publication class uses the DatabaseTable from PDO-CRUD-FOR-FREE to create an object of a row in the publications table
 *
 * Class Publication includes variables for the publications in the db,
 * Includes id, title, catagory, description, uploaded, and file link.
 * each of which are initialized from the database table publications.
 * This class includes setters and getters to receive or change the variables.
 *
 * @package Project\data
 */
class Publication extends  DatabaseTable
{
    /**
     * Publication's id
     * @var
     */
    private $id;
    /**
     * Publication's title
     * @var
     */
    private $title;
    /**
     * Publication's catagory
     * @var
     */
    private $catagory;
    /**
     * Publication's description
     * @var
     */
    private $description;
    /**
     * Publication's upload time
     * @var
     */
    private $uploaded;
    /**
     * Publication's file link
     * @var
     */
    private $fileLink;

    /**
     * Returns the Publication's file link
     * @return mixed
     */
    public function getFileLink()
    {
        return $this->fileLink;
    }

    /**
     * Sets the Publication's file link
     * @param mixed $fileLink
     */
    public function setFileLink($fileLink)
    {
        $this->fileLink = $fileLink;
    }

    /**
     * Returns the Publication's upload time
     * @return mixed
     */
    public function getUploaded()
    {
        return $this->uploaded;
    }

    /**
     * Sets the Publication's upload time
     * @param mixed $uploaded
     */
    public function setUploaded($uploaded)
    {
        $this->uploaded = $uploaded;
    }

    /**
     * Returns the Publication's id
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the Publication's id
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the Publication's title
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the Publication's title
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the Publication's catagory
     * @return mixed
     */
    public function getCatagory()
    {
        return $this->catagory;
    }

    /**
     * Sets the Publication's catagory
     * @param mixed $catagory
     */
    public function setCatagory($catagory)
    {
        $this->catagory = $catagory;
    }

    /**
     * Returns the Publication's description
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the Publication's descritpion
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}
