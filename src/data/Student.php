<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 10/04/2016
 * Time: 20:19
 */

namespace Project\data;

use Mattsmithdev\PdoCrud\DatabaseManager;
use Mattsmithdev\PdoCrud\DatabaseTable;

/**
 * Student class uses the DatabaseTable from PDO-CRUD-FOR-FREE to create an object of a row in the students table
 *
 * Class Student is designed to be a class which joins on members,
 * Includes id, memberId, and student number,
 * each of which are initialized from the database table students.
 * This class includes setters and getters to receive or change the variables.
 *
 * @package Project\data
 */
class Student extends DatabaseTable
{

    /**
     * Student ID - Primary Key
     * @var
     */
    private $id;

    /**
     * Member ID - Foreign Key
     * @var
     */
    private $memberId;

    /**
     * Student Number - eg. B00075660
     * @var
     */
    private $studentNumber;

    /**
     * Returns the Student ID
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the Student ID
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the Member ID
     * @return mixed
     */
    public function getMemberId()
    {
        return $this->memberId;
    }

    /**
     * Sets the Member ID
     * @param mixed $memberId
     */
    public function setMemberId($memberId)
    {
        $this->userId = $memberId;
    }

    /**
     * Returns the Student No
     * @return mixed
     */
    public function getStudentNumber()
    {
        return $this->studentNumber;
    }

    /**
     * Sets the Student Number
     * @param mixed $studentNumber
     */
    public function setStudentNumber($studentNumber)
    {
        $this->studentNumber = $studentNumber;
    }

}
