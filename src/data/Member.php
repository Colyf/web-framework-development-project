<?php
/**
 * Created by PhpStorm.
 * User: colin
 * Date: 10/04/2016
 * Time: 20:19
 */

namespace Project\data;

use Mattsmithdev\PdoCrud\DatabaseManager;
use Mattsmithdev\PdoCrud\DatabaseTable;

/**
 * Member class uses the DatabaseTable from PDO-CRUD-FOR-FREE to create an object of a row in the members table
 *
 * Class Member includes const variables for the role of the student,
 * Also includes id, firstname, lastname, title, username, password, gender, photo url, role and past,
 * each of which are initialized from the database table members.
 * This class includes setters and getters to receive or change the variables.
 *
 * @package Project\data
 */
class Member extends DatabaseTable
{
    /**
     * Default value for role of public user
     * @const
     */
    const ROLE_PUBLIC = 0;
    /**
     *  Default value for role of student user
     * @const
     */
    const ROLE_STUDENT = 1;
    /**
     *  Default value for role of member user
     * @const
     */
    const ROLE_MEMBER = 2;
    /**
     *  Default value role of admin user
     * @const
     */
    const ROLE_ADMIN = 3;

    /**
     * Member's id
     * @var
     */
    private $id;
    /**
     * Member's firstname
     * @var
     */
    private $firstname;
    /**
     * Member's lastname
     * @var
     */
    private $lastname;
    /**
     * Member's title
     * @var
     */
    private $title;
    /**
     * Member's gender
     * @var
     */
    private $gender;
    /**
     * Member's photoUrl
     * @var
     */
    private $photoUrl;
    /**
     * Member's username
     * @var
     */
    private $username;
    /**
     * Member's password
     * @var
     */
    private $password;
    /**
     * Member's role
     * @var
     */
    private $role;
    /**
     * Member's past
     * @var
     */
    private $past;

    /**
     * Returns the members id
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the members id
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the members firstname
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Sets the members firstname
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * Returns the members lastname
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Sets the members lastname
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Returns the members title
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the members title
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the members gender
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Sets the members gender
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * Returns the members photo url
     * @return mixed
     */
    public function getPhotoUrl()
    {
        return $this->photoUrl;
    }

    /**
     * Sets the members photo url
     * @param mixed $photoUrl
     */
    public function setPhotoUrl($photoUrl)
    {
        $this->photoUrl = $photoUrl;
    }

    /**
     * Returns the members username
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Sets the members username
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Returns the members password
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the members password after hashing
     * @param mixed $password
     */
    public function setPassword($password)
    {

        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * Returns the members role
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Sets the members role
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * Returns the members past value
     * @return mixed
     */
    public function getPast()
    {
        return $this->past;
    }

    /**
     * Sets the members past value
     * @param mixed $past
     */
    public function setPast($past)
    {
        $this->past = $past;
    }


    /**
     * return success (or not) of attempting to find matching username/password in the repo
     * @param $username
     * @param $password
     *
     * @return bool
     */
    public static function canFindMatchingUsernameAndPassword($username, $password)
    {
        $member = Member::getOneByUsername($username);

        // if no record has this username, return FALSE
        if (null == $member) {
            return false;
        }

        // hashed correct password
        $hashedStoredPassword = $member->getPassword();

        // return whether or not hash of input password matches stored hash
        return password_verify($password, $hashedStoredPassword);
    }

    /**
     * if record exists with $username, return User object for that record
     * otherwise return 'null'
     *
     * @param $username
     *
     * @return mixed|null
     */
    public static function getOneByUsername($username)
    {
        $db = new DatabaseManager();
        $connection = $db->getDbh();

        $sql = 'SELECT * FROM members WHERE username=:username';
        $statement = $connection->prepare($sql);
        $statement->bindParam(':username', $username, \PDO::PARAM_STR);
        $statement->setFetchMode(\PDO::FETCH_CLASS, __CLASS__);
        $statement->execute();

        if ($object = $statement->fetch()) {
            return $object;
        } else {
            return null;
        }
    }

    /**
     * illustrate custom PDO DB method
     * in this case we search for products with an id >= $minId, and whose descrption contains $searchText
     *
     * @param $minId
     * @param $searchText
     *
     * @return array
     */
    public static function getStudentInnerJoinMemberById($id)
    {
        $db = new DatabaseManager();
        $connection = $db->getDbh();

        $sql = 'SELECT students.*, members.*
                FROM members
                INNER JOIN students
                ON students.memberId = members.id
                WHERE members.id = '.$id.';';

        $statement = $connection->prepare($sql);
        $statement->setFetchMode(\PDO::FETCH_CLASS, '\\' . __CLASS__);
        $statement->execute();

        if ($object = $statement->fetch()) {
            return $object;
        } else {
            return null;
        }
    }
}
