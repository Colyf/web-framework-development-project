<?php
require_once __DIR__ . '/../app/setup.php';

use Project\Action\MainController;
use Project\Action\LoginController;
use Project\Action\MemberController;
use Project\Action\ProjectController;
use Project\Action\PublicationController;
use Project\Utility\Utility;


$mainController = new MainController();
$loginController = new LoginController();
$memberController = new MemberController();
$projectController = new ProjectController();
$publicationController = new PublicationController();

// get action GET parameter (if it exists)
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);

// get ID from request
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

// Standard website Controller
$app->get('/',      Utility::controller('Project\Action', 'main/index'));
$app->get('/about', Utility::controller('Project\Action', 'main/about'));
$app->get('/members', Utility::controller('Project\Action', 'main/members'));
$app->get('/projects', Utility::controller('Project\Action', 'main/projects'));
$app->get('/publications', Utility::controller('Project\Action', 'main/publications'));
$app->post('/search', Utility::controller('Project\Action', 'main/search'));
$app->get('/memberDetails/{id}', Utility::controller('Project\Action', 'main/memberDetails'));
$app->get('/projectDetails/{id}', Utility::controller('Project\Action', 'main/projectDetails'));
$app->get('/publicationDetails/{id}', Utility::controller('Project\Action', 'main/publicationDetails'));

// Login Controller
$app->post('/login', Utility::controller('Project\Action', 'login/login'));
$app->get('/logout', Utility::controller('Project\Action', 'login/logout'));
$app->post('/processLogin', Utility::controller('Project\Action', 'login/processLogin'));

// Member Controller
$app->post('/profile', Utility::controller('Project\Action', 'member/profile'));
$app->post('/profileEdit', Utility::controller('Project\Action', 'member/profileEdit'));
$app->post('/uploadPhoto', Utility::controller('Project\Action', 'member/uploadPhoto'));
$app->get('/insertMember', Utility::controller('Project\Action', 'member/insertMember'));
$app->get('/addMember', Utility::controller('Project\Action', 'member/addMemberForm'));
$app->post('/processEditMember/{id}', Utility::controller('Project\Action', 'member/processEditMember'));
$app->get('/editMember/{id}', Utility::controller('Project\Action', 'member/editMember'));
$app->get('/deleteMember/{id}', Utility::controller('Project\Action', 'member/deleteMember'));
$app->get('/pastMember/{id}', Utility::controller('Project\Action', 'member/pastMember'));

// Project Controller
$app->get('/insertProject', Utility::controller('Project\Action', 'project/insertProject'));
$app->get('/addProject', Utility::controller('Project\Action', 'project/addProjectForm'));
$app->post('/processEditProject/{id}', Utility::controller('Project\Action', 'project/processEditProject'));
$app->get('/editProject/{id}', Utility::controller('Project\Action', 'project/editProject'));
$app->get('/deleteProject/{id}', Utility::controller('Project\Action', 'project/deleteProject'));
$app->get('/pastProject/{id}', Utility::controller('Project\Action', 'project/pastProject'));

// Publicaitons Controller
$app->get('/insertPublication', Utility::controller('Project\Action', 'publication/insertPublication'));
$app->get('/addPublication', Utility::controller('Project\Action', 'publication/addPublicationForm'));
$app->post('/processEditPublication/{id}', Utility::controller('Project\Action', 'publication/processEditPublication'));
$app->get('/editPublication/{id}', Utility::controller('Project\Action', 'publication/editPublication'));
$app->get('/deletePublication/{id}', Utility::controller('Project\Action', 'publication/deletePublication'));

/*
// 404 - Page not found
$app->error(function (\Exception $e, $code) use ($app) {
    switch ($code) {
        case 404:
            $message = 'The requested page could not be found.';
            return \Project\Action\MainController::error404($app, $message);

        default:
            $message = 'We are sorry, but something went terribly wrong.';
            return \Project\Action\MainController::error404($app, $message);
    }
});
*/
// run Silex front controller
// ------------

$app['debug'] = true;
$app->run();
