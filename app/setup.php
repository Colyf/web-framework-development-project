<?php
require_once __DIR__ . '/../vendor/autoload.php';

require_once __DIR__ . '/db_config.php';

/**
 * Set up the template path used by the Controllers
 *
 * Create an environment with twig to be used by for all pages
 */
$templatePath = __DIR__ . '/../templates';

// setup Silex
// ------------
$app = new Silex\Application();


$app->register(new Silex\Provider\SessionServiceProvider());
// register Twig with Silex
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => $templatePath
));
